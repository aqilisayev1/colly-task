package config

type Config struct {
	BaseURL        string
	StartURL       string
	AllowedURLs    []string
	GeocoderAPIKey string
}

func New() *Config {
	return &Config{
		BaseURL:        "https://ymcanyc.org",
		StartURL:       "https://ymcanyc.org/locations?type&amenities",
		AllowedURLs:    []string{"www.ymcanyc.org", "ymcanyc.org"},
		GeocoderAPIKey: " ArSiVTO6txGcTGwdy4VJhIUkFy8uWIT0",
	}
}
