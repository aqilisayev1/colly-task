package collector

import (
	"github.com/gocolly/colly"
	"github.com/jasonwinn/geocoder"
	"log"
	"parserTask/config"
	"parserTask/models"
	"strings"
	"sync"
)

// Collector provides colly.Collector's expanded scraper instance
type Collector struct {
	baseUrl       string
	startUrl      string
	collector     *colly.Collector
	branchChannel chan *models.Branch
	stopper       Stopper
}

// Stopper provides mutex for blocking operation of stopping crawler
type Stopper struct {
	stopped bool
	sync.Mutex
}

// New creates a new collector with given configuration and registers OnHTML callbacks
// Sets API key for geocoder
func New(cfg *config.Config, branchChannel chan *models.Branch) *Collector {
	c := colly.NewCollector(colly.Async(true))

	c.AllowedDomains = cfg.AllowedURLs
	var collector = &Collector{
		baseUrl:       cfg.BaseURL,
		startUrl:      cfg.StartURL,
		collector:     c,
		branchChannel: branchChannel,
	}
	collector.registerOnHTMLs()

	geocoder.SetAPIKey(cfg.GeocoderAPIKey)
	return collector
}

// Start is blocking start for Collector
func Start(c *Collector) error {
	err := c.collector.Visit(c.startUrl)
	if err != nil {
		return err
	}
	c.collector.Wait()
	c.StopCrawling()
	return nil
}

// StopCrawling closes channel after finishing scraping
func (c *Collector) StopCrawling() {
	c.stopper.Lock()
	if !c.stopper.stopped {
		c.stopper.stopped = true
		close(c.branchChannel)
	}
	c.stopper.Unlock()
}

// registerOnHTMLs assigns OnHTML callbacks to colly.Collector
func (c *Collector) registerOnHTMLs() {

	// Branch scraping callback
	c.collector.OnHTML("div.location-list-item", func(element *colly.HTMLElement) {
		title := element.ChildText("h2.location-item--title")
		address := element.ChildText("div.field-location-direction")
		phoneNumber := element.ChildText("div.field-location-phone a")
		link := element.ChildAttr(".branch-view-button a", "href")
		lat, long, err := geocoder.Geocode(address)
		if err != nil {
			log.Println(err)
		}

		branch := models.Branch{
			Name:        title,
			Address:     address,
			PhoneNumber: phoneNumber,
			Link:        link,
			Lat:         lat,
			Long:        long,
		}

		ctx := colly.NewContext()
		ctx.Put("branch", &branch)

		err = c.collector.Request("GET", c.baseUrl+link+"/about", nil, ctx, nil)
		if err != nil {
			log.Println(err)
		}
	})

	// Staff containers scraping callback
	// "div.left-col" can scrape 18 pages
	// No other unique identifier for employee container and inconsistent structure of html to write hard code path
	c.collector.OnHTML("div.left-col", func(element *colly.HTMLElement) {
		branch := element.Request.Ctx.GetAny("branch").(*models.Branch)

		c.getEmployeeInfoFromContainer(element, branch)

		c.branchChannel <- branch
	})
}

// getEmployeeInfoFromContainer appends scraped employees from container to model.Branch's slice
func (c *Collector) getEmployeeInfoFromContainer(element *colly.HTMLElement, branch *models.Branch) {
	element.ForEach("p", func(i int, element *colly.HTMLElement) {

		// To prevent empty p tags
		if len(element.Text) > 2 {

			lines := strings.Split(element.Text, "\n")

			// check if element consists of multiple lines
			// some pages contains only title "p" tags, not belonging to staff
			if len(lines) > 1 {
				name := lines[0]
				mail := element.ChildText("a")
				title := strings.Replace(lines[1], mail, "", 1)
				number := ""
				postalCode := ""

				// Check if "p" tag contains phone and postal code line(always 3rd line)
				if len(lines) == 3 {
					numberAndPostal := strings.Split(strings.ToLower(lines[2]), "x")
					number = numberAndPostal[0]
					if len(numberAndPostal) == 2 {
						postalCode = numberAndPostal[1]
					}
				}

				emp := models.Employee{
					Name:        name,
					Email:       mail,
					PhoneNumber: number,
					Title:       title,
					PostalCode:  postalCode,
				}
				branch.Employees = append(branch.Employees, emp)
			}
		}
	})
}
