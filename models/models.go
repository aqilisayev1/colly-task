package models

import "encoding/json"

// Branch provides branch details and scraped employees belonging to branch
type Branch struct {
	Name        string     `json:"name"`
	Address     string     `json:"address"`
	PhoneNumber string     `json:"phone_number"`
	Link        string     `json:"link"`
	Employees   []Employee `json:"employees"`
	Lat         float64    `json:"lat"`
	Long        float64    `json:"long"`
}

// Implemented Stringer interface with json format
func (b *Branch) String() string {
	str, _ := json.MarshalIndent(b, "", "  ")
	return string(str)
}

// Employee provides employee details
type Employee struct {
	Name        string `json:"name"`
	Title       string `json:"title"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
	PostalCode  string `json:"postal_code"`
}
