package exporter

import (
	"fmt"
	"parserTask/models"
)

// Printer exports given data to command line
type Printer struct{}

// NewPrinter creates a new exporter
func NewPrinter() Exporter {
	return Printer{}
}

// Exports given data to command line
func (p Printer) SaveBranch(branch *models.Branch) error {
	fmt.Println(branch)
	return nil
}
