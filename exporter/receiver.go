package exporter

import (
	"log"
	"parserTask/models"
)

// Exporter is a interface for reading scraped data from channel
// and exporting it to given implementation
type Exporter interface {
	SaveBranch(branch *models.Branch) error
}

// Start starts a for range loop on given channel and exports received data until channel is closed
func Start(exporter Exporter, branchChannel chan *models.Branch) {
	for branch := range branchChannel {
		err := exporter.SaveBranch(branch)
		if err != nil {
			log.Println(err)
		}
	}
}
