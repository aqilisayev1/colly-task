package exporter

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"parserTask/models"
)

// JsonFile struct that provides exporting scraped data to json file
type JsonFile struct {
	fileName string
}

// NewJsonFile creates a new exporter
// Checks if file with given name doesn't exist then creates new one and writes "[]" to it
func NewJsonFile(fileName string) (Exporter, error) {
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		file, err := os.Create(fileName)
		if err != nil {
			return nil, err
		}
		_, err = file.WriteString("[]")
		if err != nil {
			return nil, err
		}
	}
	return &JsonFile{fileName: fileName}, nil
}

// Appends the new scraped branch to json file
func (j JsonFile) SaveBranch(branch *models.Branch) error {
	data, err := j.getData()

	if err != nil {
		return err
	}

	data = append(data, *branch)

	err = j.saveData(data)
	if err != nil {
		return err
	}

	return nil
}

// Reading current data in json file
func (j JsonFile) getData() ([]models.Branch, error) {

	file, err := ioutil.ReadFile(j.fileName)
	if err != nil {
		return nil, err
	}

	var data []models.Branch

	err = json.Unmarshal(file, &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// Saving given data to json file
func (j JsonFile) saveData(data []models.Branch) error {
	dataBytes, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(j.fileName, dataBytes, 0644)
	if err != nil {
		return err
	}
	return nil
}
