package main

import (
	"parserTask/collector"
	"parserTask/config"
	"parserTask/exporter"
	"parserTask/models"
)

func main() {
	cfg := config.New()
	branchChannel := make(chan *models.Branch)
	c := collector.New(cfg, branchChannel)
	e := exporter.NewPrinter()

	go exporter.Start(e, branchChannel)

	if err := collector.Start(c); err != nil {
		panic(err)
	}
}
